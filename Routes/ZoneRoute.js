/**
 * Created by SONY on 10/9/2016.
 */
'use strict';

const Joi = require('joi');
const UniversalFunction = require('../Utils/UniversalFunctions');
const Config = require('../Config');
const Controllers = require('../Controllers');

const addZone = {
    method: 'POST',
    path: '/api/addZone',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.addZone(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Add Zone',
        tags: ['api', 'admin'],
        validate: {
            payload: {
                name: Joi.string().min(2).trim().required()
            },
            failAction: UniversalFunction.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const addConstituency = {
    method: 'POST',
    path: '/api/addConstituency',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.addConstituency(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Add Constituency',
        tags: ['api'],
        validate: {
            payload: {
                name: Joi.string().min(2).trim().required(),
                zoneName: Joi.string().optional().trim().allow(''),
                zoneId: Joi.string().optional().allow('')
            },
            failAction: UniversalFunction.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const addCandidate = {
    method: 'POST',
    path: '/api/addCandidate',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.addCandidate(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Add Candidate',
        tags: ['api'],
        /*    payload: {
         maxBytes: 2000000,
         output: 'file',
         parse: true
         },*/
        validate: {
            payload: {
                zoneName: Joi.string().trim().required(),
                constituencyName: Joi.string().trim().required(),
                nameEng: Joi.string().trim().required(),
                namePun: Joi.string().trim().required(),
                phoneNumber: Joi.string().trim().required(),
                info: Joi.string().trim().required(),
                cmNameEng: Joi.string().trim().required(),
                cmNamePun: Joi.string().trim().required(),
                cmPhoneNumber: Joi.string().trim().required(),
                /*   profilePic: Joi.any()
                 .meta({swaggerType: 'file'})
                 .optional()
                 .allow('')
                 .description('Image file')*/
            },
            failAction: UniversalFunction.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getAllZones = {
    method: 'GET',
    path: '/api/getZones',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.getAllZones((err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Get all Zones',
        tags: ['api', 'admin'],
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getConstituencies = {
    method: 'GET',
    path: '/api/getConstituency',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.getConstituencies(request.query, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Get Constituency',
        tags: ['api', 'admin'],
        validate: {
            query: {
                name: Joi.string().trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const getCandidate = {
    method: 'GET',
    path: '/api/getCandidate',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.getCandidate(request.query, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Get Candidate',
        tags: ['api', 'admin'],
        validate: {
            query: {
                zoneName: Joi.string().trim().required(),
                constituencyName: Joi.string().trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const addManifestoPts = {
    method: 'POST',
    path: '/api/addManifestoPts',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.addManifesto(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Add Manifesto Points',
        tags: ['api', 'admin'],
        validate: {
            payload: {
                manifestoType: Joi.string().valid(["TRADE", "KISAN", "YOUTH", "DALIT"]).required(),
                manifestoArrayEng: Joi.array().items(Joi.string()),
                manifestoArrayPun: Joi.array().items(Joi.string())
            },
            failAction: UniversalFunction.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const addVolunteer = {
    method: 'POST',
    path: '/api/addVolunteer',
    handler: (request, reply) => {
        Controllers.ZoneCtrl.addVolunteer(request.payload, (err, data) => {
            if (err) {
                reply(UniversalFunction.sendError(err));
            } else {
                reply(UniversalFunction.sendSuccess(null, data));
            }
        })
    },
    config: {
        description: 'Add Volunteer',
        tags: ['api'],
        validate: {
            payload: {
                fullName: Joi.string().trim().required(),
                emailId: Joi.string().trim().required(),
                phoneOne: Joi.string().trim().required(),
                phoneTwo: Joi.string().trim(),
                arrivalDate: Joi.string().trim(),
                departureDate: Joi.string().trim(),
                prefConstituency: Joi.string().trim()
            },
            failAction: UniversalFunction.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    addZone,
    addConstituency,
    addCandidate,
    getAllZones,
    getConstituencies,
    getCandidate,
    addManifestoPts,
    addVolunteer
];