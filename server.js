'use strict';

const Hapi = require('hapi');
const plugins = require('./Plugins');
const routes = require('./Routes');
var Config = require('./Config');
var MongoConnect = require('./Utils/MongoConnect');

// Create a server with a host and port
var server = new Hapi.Server(
    {
        app: {Name: Config.AppConstants.SERVER.AppName}
    }
);
server.connection({
    port: Config.AppConstants.SERVER.Ports.HAPI,
    //port: process.env.ports || 5000,
    host: Config.AppConstants.SERVER.Ports.host,
    routes: {cors: true}
});

//Register All Plugins
server.register(plugins, function (err) {
    if (err) {
        server.error('Error while loading Plugins : ' + err)
    } else {
        server.log('info', 'Plugins Loaded')
    }
});

server.route(routes);

server.start(function (err) {
    server.log('info', 'Server running at: ' + server.info.uri);
});
