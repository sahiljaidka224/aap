/**
 * Created by cl-macmini-88 on 10/10/16.
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ManifestoSchema = new Schema({

    manifestoType: {type: String, enum: ["TRADE", "KISAN", "YOUTH", "DALIT"]},
    manifestoArrayEng: {type: [String]},
    manifestoArrayPun: {type: [String]}

}, {timestamps: true});

module.exports = mongoose.model('ManifestoSchema', ManifestoSchema);