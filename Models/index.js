/**
 * Created by SONY on 10/9/2016.
 */

module.exports = {
    ZoneSchema: require('./ZoneSchema'),
    ConstituencySchema: require('./ConstituencySchema'),
    CandidateSchema: require('./CandidateSchema'),
    ManifestoSchema: require('./ManifestoSchema'),
    VolunteerSchema: require('./VolunteerSchema')
};