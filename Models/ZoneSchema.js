/**
 * Created by SONY on 10/9/2016.
 */

'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ZoneSchema = new Schema({

    name: {type: String, default: null}

}, {timestamps: true});

module.exports = mongoose.model('ZoneSchema', ZoneSchema);