/**
 * Created by SONY on 10/9/2016.
 */

'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConstituencySchema = new Schema({

    name: {type: String, default: null},
    zoneName: {type: String, default: null},
    zoneId: {type: Schema.ObjectId, ref: 'ZoneSchema'}

}, {timestamps: true});

module.exports = mongoose.model('ConstituencySchema', ConstituencySchema);
