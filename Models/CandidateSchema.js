/**
 * Created by SONY on 10/9/2016.
 */

'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CandidateSchema = new Schema({

    zoneName: {type: String, default: null},
    zoneId: {type: Schema.ObjectId, ref: 'ZoneSchema'},

    constituencyName: {type: String, default: null},
    constituencyId: {type: Schema.ObjectId, ref: 'ConstituencySchema'},

    nameEng: {type: String, default: null, trim: true},
    namePun: {type: String, default: null, trim: true},
    phoneNumber: {type: String, default: null},
    info: {type: String, default: null},
    cmNameEng: {type: String, default: null, trim: true},
    cmNamePun: {type: String, default: null, trim: true},
    cmPhoneNumber: {type: String, default: null, trim: true},

    profilePic: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    }


}, {timestamps: true});

module.exports = mongoose.model('CandidateSchema', CandidateSchema);