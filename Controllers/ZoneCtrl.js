/**
 * Created by SONY on 10/9/2016.
 */
'use strict';

const Schema = require('../Models');
const async = require('async');
const Config = require('../Config');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const UniversalFunctions = require('../Utils/UniversalFunctions');
const json2csv = require('json2csv');
const fs = require('fs');
const csv = require("fast-csv");

const addZone = (payload, callbackRoute) => {
    let dataToSave = payload;
    dataToSave.name = payload.name.toUpperCase();
    let zoneData = {};

    async.series({
        'checkIfAlreadyExists': (cb) => {
            let criteria = {
                name: payload.name.toUpperCase()
            };
            Schema.ZoneSchema.find(criteria, {}, {lean: true, new: true}, (err, data)=> {
                if (err) {
                    return cb(err);
                } else {
                    if (data && data.length != 0) {
                        return cb(ERROR.ZONE_EXISTS);
                    } else {
                        return cb(null);
                    }
                }
            })
        },
        'addInDB': (cb) => {
            Schema.ZoneSchema(dataToSave).save((err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    zoneData = data;
                    return cb(null);
                }
            })
        }

    }, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            //zoneData.name = zoneData.name[0].toUpperCase() + zoneData.name.slice(1);
            return callbackRoute(null, zoneData);
        }
    })
};

const addConstituency = (payload, callbackRoute) => {

    let dataToSave = payload;
    let constituencyData = {};

    dataToSave.zoneName = payload.zoneName.toUpperCase();
    dataToSave.name = payload.name.toUpperCase();

    async.series({
        'checkOnPayload': (cb) => {
            if (!dataToSave.zoneId) {
                if (!dataToSave.zoneName) {
                    return cb("Zone Id or Zone Name, One of them is required.");
                } else {
                    return cb(null);
                }
            }
            else if (dataToSave.zoneName) {
                return cb("Only one of the both: Zone Id or Zone name is required");
            }
            else {
                return cb(null);
            }
        },

        'checkForZone': (cb) => {
            let criteria = {};

            if (dataToSave.zoneName) {
                criteria.name = payload.zoneName.toUpperCase()
            }
            if (dataToSave.zoneId) {
                criteria._id = payload.zoneId;
            }
            console.log("criteria +++", criteria);
            Schema.ZoneSchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    console.log("DATA ++++", data);
                    if (data && data.length != 0) {
                        return cb(null);
                    } else {
                        return cb("Invalid Zone");
                    }
                }
            })
        },

        'checkIfAlreadyExists': (cb) => {
            let criteria = {};
            criteria.name = payload.name.toUpperCase();

            if (dataToSave.zoneName) {
                criteria.zoneName = payload.zoneName.toUpperCase()
            }
            if (dataToSave.zoneId) {
                criteria.zoneId = payload.zoneId;
            }

            Schema.ConstituencySchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    if (data && data.length != 0) {
                        return cb(ERROR.CONSTITUENCY_EXISTS);
                    } else {
                        return cb(null);
                    }
                }
            })
        },

        'addInDB': (cb) => {
            Schema.ConstituencySchema(dataToSave).save((err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    constituencyData = data;
                    return cb(null);
                }
            })
        }

    }, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            return callbackRoute(null, constituencyData);
        }
    })
};

const addCandidate = (payload, callbackRoute) => {

    let dataToSave = payload;
    let candidateData = {};

    dataToSave.zoneName = payload.zoneName.toUpperCase();
    dataToSave.constituencyName = payload.constituencyName.toUpperCase();

    async.series({
        'checkIfZoneExists': (cb) => {
            let criteria = {
                name: payload.zoneName.toUpperCase()
            };

            Schema.ZoneSchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    if (data && data.length != 0) {
                        return cb(null);
                    } else {
                        return cb("Invalid Zone Name");
                    }
                }
            })
        },

        'checkIfConstituencyExists': (cb) => {
            let criteria = {
                name: payload.constituencyName.toUpperCase()
            };

            Schema.ConstituencySchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    if (data && data.length != 0) {
                        return cb(null);
                    } else {
                        return cb("Invalid Constituency Name");
                    }
                }
            })
        },

        'checkIfCandidateExists': (cb) => {
            let criteria = {
                $and: [{zoneName: payload.zoneName.toUpperCase()}, {constituencyName: payload.constituencyName.toUpperCase()},
                    {nameEng: payload.nameEng.toUpperCase()}, {namePun: payload.namePun}]
            };

            Schema.CandidateSchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    if (data && data.length != 0) {
                        return cb("Candidate Already Exists");
                    } else {
                        return cb(null);
                    }
                }
            })
        },

        'addInDb': (cb) => {
            Schema.CandidateSchema(dataToSave).save((err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    candidateData = data;
                    return cb(null);
                }
            })
        }
    }, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            return callbackRoute(null);
        }
    })

};

const getAllZones = (callbackRoute) => {
    Schema.ZoneSchema.find({}, {}, {lean: true, new: true}, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            return callbackRoute(null, data);
        }
    })
};

const getConstituencies = (payload, callbackRoute) => {
    Schema.ConstituencySchema.find({name: payload.name.toUpperCase()}, {}, {lean: true, new: true}, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            return callbackRoute(null, data);
        }
    })
};

const getCandidate = (payload, callbackRoute) => {
    let criteria = {
        $and: [{zoneName: payload.zoneName.toUpperCase()}, {constituencyName: payload.constituencyName.toUpperCase()}]
    };
    Schema.CandidateSchema.find(criteria, {}, {lean: true, new: true}, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            return callbackRoute(null, data);
        }
    })
};

const addManifesto = (payload, callbackRoute) => {
    let dataToSave = payload;
    let manifestoData = {};

    async.series({

        'addInDB': (cb) => {
            Schema.ManifestoSchema(payload).save((err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    manifestoData = data;
                    return cb(null);
                }
            })
        }

    }, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            //zoneData.name = zoneData.name[0].toUpperCase() + zoneData.name.slice(1);
            return callbackRoute(null, manifestoData);
        }
    })
};

const addVolunteer = (payload, callbackRoute) => {

    let dataToSave = payload;
    let volunteerData = {};
    let fields = ["fullName", "emailId", "phoneOne", "phoneTwo", "arrivalDate", "departureDate", "prefConstituency"];
    let fieldNames = ["fullName", "emailId", "phoneOne", "phoneTwo", "arrivalDate", "departureDate", "prefConstituency"];

    async.series({

        'addInDB': (cb) => {
            Schema.VolunteerSchema(dataToSave).save((err, data) => {
                if (err) {
                    return cb(err);
                } else {
                    volunteerData = data;

                    let csvFile = json2csv({data: dataToSave, fields: fields});



                    fs.appendFile('file.csv', csvFile, function (err) {
                        if (err) throw err;
                        console.log('file saved');
                    });
                    return cb(null);
                }
            })
        }

    }, (err, data) => {
        if (err) {
            return callbackRoute(err);
        } else {
            //zoneData.name = zoneData.name[0].toUpperCase() + zoneData.name.slice(1);
            return callbackRoute(null, volunteerData);
        }
    })
};


module.exports = {
    addZone: addZone,
    addConstituency: addConstituency,
    addCandidate: addCandidate,
    getAllZones: getAllZones,
    getConstituencies: getConstituencies,
    getCandidate: getCandidate,
    addManifesto: addManifesto,
    addVolunteer: addVolunteer
};